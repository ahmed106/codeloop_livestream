<style>

.statistics {
  display: flex;
  align-items: center;
  /* justify-content: space-between; */
  padding: 20px;
  gap: 30px;
}

.statistics p {
  margin: 0;
  font-size: 20px;
  display: flex;
  align-items: center;
}

.statistics p span {
  display: inline-block;
  margin-right: 15px;
  font-size: 30px;
  font-weight: bold;
}

.statistics .leave-live {
  border: 1px solid #fa6342;
  padding: 7px 30px;
  border-radius: 50px;
  font-size: 17px;
  font-family: 'Bahij_Plain';
  background: #fa6342;
  color: #fff;
  cursor: pointer;
}

.flex-live {
  display: flex;
  gap: 40px;

}

.live-div {
  width: 100%;
}

.live-div .iframe-live {
  border: 0;
  height: 100%;
  min-height: 500px;
}

.chat-div {
  min-width: 500px;
  width: 500px;
  border: 1px solid #e0e0e0;
}

.header-chat {
  padding: 10px;
  border-bottom: 1px solid #e0e0e0;
}

.header-chat h4 {
  margin: 0;
}

.chat-content-live {
  margin: 0;
  list-style: none;
  padding: 15px;
  border-bottom: 2px solid #edf2f6;
  height: 392px;
  max-height: 392px;
  overflow: auto;
}

.chat-content-live li {
  margin-bottom: 10px;
}

.chat-content-live li a {
  display: flex;
  gap: 8px;
}

.chat-content-live li a img {
  width: 30px;
  min-width: 30px;
  height: 30px;
  min-height: 30px;
  border-radius: 50%;
}

.chat-content-live li a .chat-desc {
  width: 100%;
  display: flex;
  flex-direction: column;
}

.chat-content-live li a h5 {
  margin: 0;
  color: #2a3c53;
}

.chat-content-live li a .chat-desc {
  padding: 5px 10px;
  border-radius: 10px 0 10px 10px;
  position: relative;
  background: #ffd9d9;
  color: #262626;
}

.chat-content-live li a .chat-desc p {
  margin: 0;
  line-height: 21px;
}

.chat-content-live li a .chat-desc::before {
  width: 0;
  height: 0;
  content: "";
  top: 0;
  right: -10px;
  border-style: solid;
  border-width: 13px 13px 0 0;
  border-color: #ffd9d9 transparent transparent transparent;
  position: absolute;
}

.chat-content-live li.reverse-chat a{
flex-direction: row-reverse;
}

.chat-content-live li.reverse-chat .chat-desc {
  background: #f1f1f1;
  color: #030303;
  border-radius: 0 10px 10px 10px;
}

.chat-content-live li.reverse-chat .chat-desc::before {
  content: "";
  top: 0;
  left: -10px;
  right: auto;
  border-style: solid;
  border-width: 13px 0 0 13px;
  border-color: #f1f1f1 transparent transparent transparent;
}

.footer-chat {
padding: 15px;
display: flex;
align-items: center;
position: relative;
gap: 10px;
}

.footer-chat input{
  border: 1px solid #9eadc3b0;
  background: #fff;
  height: 40px;
  border-radius: 50px;
  width: 100%;
  padding: 10px 20px;
  font-size: 15px;
}

.footer-chat button{
  border: 0;
  background: unset;
  color: #2a3c53;
  border-radius: 50%;
  font-family: inherit;
  font-size: 22px;
  cursor: pointer;
  min-width: 36px;
  margin: auto;
  display: flex;
  align-items: center;
  justify-content: center;
}

.footer-chat button i{
  position: unset;
}

</style>