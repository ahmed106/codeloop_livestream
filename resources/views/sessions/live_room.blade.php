<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>live</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Kufi+Arabic&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
          referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
    <style>
        *,
        ::after,
        ::before {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        ::-webkit-scrollbar {
            width: 8px;
            height: 8px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background: #949fad;
        }

        html {
            font-size: 62.5%;
            font-size-adjust: 100%;
            font-weight: 400;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        body {
            margin: 0;
            overflow-x: hidden;
            font-family: 'Noto Kufi Arabic', sans-serif;
            font-size: 1.4rem;
            line-height: 1.6;
            color: #103178;
            background: #edf2f6;
            direction: rtl;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-weight: 700;
            line-height: 1.2;
            color: #103178;
            letter-spacing: 0;
        }

        h5 {
            font-size: 1.5rem;
        }

        h4 {
            font-size: 1.8rem;
        }

        p {
            font-size: 1.4rem;
        }

        .container {
            width: 100%;
            padding-right: 20px;
            padding-left: 20px;
            margin-right: auto;
            margin-left: auto;
            max-width: 100%;
        }

        .card-custom {
            padding: 15px;
            background: #fff;
            border-radius: 10px;
            margin: 5px;
        }

        .statistics {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 20px 0;
            padding-top: 0;
            gap: 30px;
        }

        .statistics p {
            margin: 0;
            font-size: 20px;
            display: flex;
            align-items: center;
        }

        .statistics p span {
            display: inline-block;
            margin-right: 15px;
            font-size: 30px;
            font-weight: bold;
        }

        .statistics .leave-live {
            border: 1px solid #fa6342;
            padding: 3px 30px;
            border-radius: 50px;
            font-size: 17px;
            font-family: inherit;
            background: #fa6342;
            color: #fff;
            cursor: pointer;
        }

        .flex-live {
            display: flex;
            gap: 40px;

        }

        .live-div {
            width: 100%;
        }

        .live-div .OT_publisher,.live-div .OT_subscriber {
            border: 0;
            height: 100%;
            min-height: 600px;
        }

        .chat-div {
            min-width: 600px;
            width: 600px;
            border: 1px solid #e0e0e0;
        }

        .header-chat {
            padding: 10px;
            border-bottom: 1px solid #e0e0e0;
        }

        .header-chat h4 {
            margin: 0;
        }

        .chat-content-live {
            margin: 0;
            list-style: none;
            padding: 15px;
            border-bottom: 2px solid #edf2f6;
            height: 492px;
            max-height: 492px;
            overflow: auto;
        }

        .chat-content-live li {
            margin-bottom: 10px;
        }

        .chat-content-live li a {
            display: flex;
            gap: 8px;
            text-decoration: none
        }

        .chat-content-live li a img {
            width: 30px;
            min-width: 30px;
            height: 30px;
            min-height: 30px;
            border-radius: 50%;
            max-width: 100%;
            -o-object-fit: cover;
            object-fit: cover;
            vertical-align: middle;
        }

        .chat-content-live li a .chat-desc {
            padding: 5px 10px;
            border-radius: 10px 0 10px 10px;
            position: relative;
            background: #ffd0d0;
            color: #262626;
        }

        .chat-content-live li a .chat-desc::before {
            width: 0;
            height: 0;
            content: "";
            top: 0;
            right: -10px;
            border-style: solid;
            border-width: 13px 13px 0 0;
            border-color: #ffd0d0 transparent transparent transparent;
            position: absolute;
        }

        .chat-content-live li a .chat-desc h5 {
            margin: 0;
            color: #2a3c53;
        }

        .chat-content-live li a .chat-desc p {
            margin: 0;
            line-height: 21px;
        }

        .chat-content-live li.reverse-chat a {
            flex-direction: row-reverse;
        }

        .chat-content-live li.reverse-chat .chat-desc {
            background: #e4e8fb;
            color: #030303;
            border-radius: 0 10px 10px 10px;
        }

        .chat-content-live li.reverse-chat .chat-desc::before {
            content: "";
            top: 0;
            left: -10px;
            right: auto;
            border-style: solid;
            border-width: 13px 0 0 13px;
            border-color: #e4e8fb transparent transparent transparent;
        }

        .footer-chat {
            padding: 15px;
            display: flex;
            align-items: center;
            position: relative;
            gap: 10px;
        }

        .footer-chat input {
            border: 1px solid #9eadc3b0;
            background: #fff;
            height: 40px;
            border-radius: 50px;
            width: 100%;
            padding: 10px 20px;
            font-size: 15px;
            font-family: inherit;
            outline: unset;
        }

        .footer-chat button {
            border: 0;
            background: unset;
            color: #2a3c53;
            border-radius: 50%;
            font-family: inherit;
            font-size: 22px;
            cursor: pointer;
            min-width: 36px;
            margin: auto;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .footer-chat button i {
            position: unset;
        }

        @media (max-width: 1200px) {
            .statistics p span {
                margin-right: 10px;
                font-size: 25px;
            }

            .statistics {
                gap: 20px;
                padding-top: 0px;
            }

            .statistics .leave-live {
                padding: 5px 30px;
            }

            .chat-div {
                min-width: 400px;
                width: 400px;
            }

            .flex-live {
                gap: 20px;
            }

        }


        @media (max-width: 992px) {
            .flex-live {
                flex-direction: column;
            }

            .live-div .OT_publisher,.live-div .OT_subscriber {
                min-height: 400px;
            }

            .chat-div {
                min-width: 100%;
                width: 100%;
            }

            .statistics p {
                font-size: 18px;
            }

            .statistics p span {
                font-size: 20px;
            }

            .statistics .leave-live {
                padding: 4px 25px;
                font-size: 16px;
            }

            .live-div .OT_publisher,.live-div .OT_subscriber {
                min-height: 400px;
            }

        }

        @media (max-width: 576px) {

            .live-div .OT_publisher,.live-div .OT_subscriber {
                min-height: 200px;
            }

            .chat-content-live li a .chat-desc h5 {
                font-size: 14px;
            }

            .header-chat {
                padding: 6px 10px;
            }

            .header-chat h4 {
                font-size: 17px;
            }

            .flex-live {
                gap: 8px;
            }

            .chat-content-live li a .chat-desc p {
                line-height: 18px;
                font-size: 13px;
            }

            .footer-chat {
                padding: 10px;
            }

            .footer-chat input {
                height: 34px;
                padding: 5px 15px;
            }

            .statistics {
                gap: 10px;
                flex-wrap: wrap;
            }

            .container {
                padding-right: 0;
                padding-left: 0;
            }

            .chat-content-live {
                height: calc(100vh - 393px);
                max-height: calc(100vh - 393px);
            }

        }

        @media (max-width: 400px) {
            .statistics .leave-live {
                padding: 3px 15px;
                font-size: 15px;
            }
        }
    </style>
</head>

<body>
<div class="container">
    <div class="card-custom">
        <div class="statistics">
            <p>عدد المشاهدين <span id="session_members"></span></p>
            <button class="leave-live" id="disconnect">مغادرة</button>
        </div>
        <div class="flex-live">
            <div class="live-div">

                <div id="subscriber"></div>
                <div id="publisher"></div>


            </div>
            <div class="chat-div">
                <div class="header-chat">
                    <h4>المحادثات</h4>
                </div>
                <ul class="chat-content-live chat_box">


                </ul>
                <div class="footer-chat">
                    <input class="input-message" type="text" placeholder="اكتب هنا">
                    <button id="sendMessage"><i class="fa fa-paper-plane"></i></button>
                </div>
            </div>
        </div>
    </div>

</div>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" referrerpolicy="no-referrer"></script>
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" ></script>
<script>
    // replace these values with those generated in your TokBox Account
    var apiKey = "{!! config('opentok.api_key') !!}";
    var sessionId = "{!! $sessionID !!}";
    var token = "{!! $token !!}";

    // Handling all of our errors here by alerting them
    function handleError(error) {
        if (error) {
            alert(error.message);
        }
    }

    // (optional) add server code here
    initializeSession();


    function initializeSession() {
        var session = OT.initSession(apiKey, sessionId);
        recievedMessages(session)

        $('#sendMessage').on('click', function (e) {
            e.preventDefault();

                if($('.input-message').val()!='' ){
                    sendMessage(session)
                }else{

                    $('.input-message').focus()
                }
          
        })
        // Subscribe to a newly created stream
        session.on('streamCreated', function (event) {

            session.subscribe(event.stream, 'subscriber', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
            }, handleError);
        });

        $('#disconnect').on('click', function () {

            disconnect(session)
        })

        $('.input-message').on('keyup',function(e){
            if(e.keyCode==13){
                if($('.input-message').val()!='' ){
                    sendMessage(session)
                }
            }
        })

        // Connect to the session
        session.connect(token, function (error) {
            // If the connection is successful, initialize a publisher and publish to the session
            if (error) {
                handleError(error);
            } else {
                if (session.capabilities.publish) {

                    // Create a publisher
                    var publisher = OT.initPublisher('publisher', {
                        insertMode: 'append',
                        width: '100%',
                        height: '100%'
                    }, handleError);
                    session.publish(publisher, handleError);
                }else{

                }

            }
        });

        var connectionCount = 0;
        session.on("connectionCreated", function (event) {
            if (session.capabilities.publish) {
                toastr['success'](JSON.parse(event.connection.data).name + ' Joined live')
            }
            connectionCount++;
            displayConnectionCount();
        });
        session.on("connectionDestroyed", function (event) {

            if (event.connection.permissions.publish) {
                toastr['error'](JSON.parse(event.connection.data).name + ' has left');
                disconnect(session)
            }
            toastr['error'](JSON.parse(event.connection.data).name + ' has left');
            connectionCount--;
            displayConnectionCount();
        });

        function displayConnectionCount() {

            $('#session_members').html(connectionCount.toString())

        }


    }

    function sendMessage(session) {

        session.signal(
            {
                data: $('.input-message').val(),
            },
            function (error) {
                if (error) {
                    console.log("signal error ("
                        + error.name
                        + "): " + error.message);
                } else {

                    console.log("signal sent. ");
                }
            }
        );
    }

    function recievedMessages(session) {

        session.on("signal", function (event) {

            let _class = '';
            let html = '';
            if (event.from.id == session.connection.id) {

       html = `
                   <li>
                        <a href="#">
                            <img src="https://wastah.store/backend/public/images/users/164415763839385.jpg"
                                 alt="">
                                <div class="chat-desc">
                                    <h5>${JSON.parse(event.from.data).name}</h5>
                                    <p>${event.data}</p>
                                </div>
                        </a>
                    </li>
                    `;
                


            } else {

                html = `
                    <li class="reverse-chat">
                        <a href="#">
                            <img src="https://wastah.store/backend/public/images/users/164415763839385.jpg" alt="">
                            <div class="chat-desc">
                                <h5>
                                ${JSON.parse(event.from.data).name}
                                </h5>
                                <p>${event.data}</p>
                            </div>
                        </a>
                    </li>

                    `;

            }


            $('.chat_box').append(html);
            $('.chat_box').scrollTop($('.chat_box')[0].scrollHeight);
            $('.input-message').val('');
        });

    }

    function disconnect(session) {


      //  toastr['success']('successsss', 'success')


        session.disconnect()

        window.location.href = '{!! url('/sessions') !!}'
    }

</script>
</html>
