    
    @extends('layouts.master')
    @include('includes.live_room_style')
    @section('content')
    <div class="container">
        <div class="statistics">
            <p>عدد المشاهدين <span id="session_members"></span></p>

            <a href="#"  id="disconnect" class="leave-live">مغادرة</a>
           
        </div>
        <div class="flex-live">
            <div class="live-div">

                <div  class="" width="100%" height="500" id="subscriber"></div>
                <div class="iframe-live" width="100%" height="500" id="publisher"></div>

                
            </div>
            <div class="chat-div">
                <div class="header-chat">
                    <h4>المحادثات</h4>
                </div>
                <ul class="chat-content-live chat_box">
                   
                   
                </ul>
                <div class="footer-chat">
                    <input class="input-message" type="text" placeholder="اكتب هنا">
                    <button id="sendMessage"><i class="fa fa-paper-plane"></i></button>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('javascript')
    <script src="https://static.opentok.com/v2/js/opentok.min.js"></script>

    <script>
        // replace these values with those generated in your TokBox Account
        var apiKey = "{!! config('opentok.api_key') !!}";
        var sessionId = "{!! $sessionID !!}";
        var token = "{!! $token !!}";

        // Handling all of our errors here by alerting them
        function handleError(error) {
            if (error) {
                alert(error.message);
            }
        }

        // (optional) add server code here
        initializeSession();


        function initializeSession() {
            var session = OT.initSession(apiKey, sessionId);
            recievedMessages(session)

            $('#sendMessage').on('click', function (e) {
                e.preventDefault();

                sendMessage(session)
            })
            // Subscribe to a newly created stream
            session.on('streamCreated', function (event) {

                session.subscribe(event.stream, 'subscriber', {
                    insertMode: 'append',
                    width: '100%',
                    height: '100%'
                }, handleError);
            });

            $('#disconnect').on('click', function () {

                disconnect(session)
            })

           $('.input-message').on('keyup',function(e){
                if(e.keyCode==13){
                    sendMessage(session)
                }
           })

            // Connect to the session
            session.connect(token, function (error) {
                // If the connection is successful, initialize a publisher and publish to the session
                if (error) {
                    handleError(error);
                } else {
                    if (session.capabilities.publish) {
                      
                        // Create a publisher
                        var publisher = OT.initPublisher('publisher', {
                            insertMode: 'append',
                            width: '100%',
                            height: '100%'
                        }, handleError);
                        session.publish(publisher, handleError);
                    }else{
                       
                    }

                }
            });

            var connectionCount = 0;
            session.on("connectionCreated", function (event) {
                if (session.capabilities.publish) {
                    toastr['success'](JSON.parse(event.connection.data).name + ' Joined live')
                }
                connectionCount++;
                displayConnectionCount();
            });
            session.on("connectionDestroyed", function (event) {

                if (event.connection.permissions.publish) {
                    toastr['error'](JSON.parse(event.connection.data).name + ' has left');
                    disconnect(session)
                }
                toastr['error'](JSON.parse(event.connection.data).name + ' has left');
                connectionCount--;
                displayConnectionCount();
            });

            function displayConnectionCount() {

                $('#session_members').html(connectionCount.toString())

            }


        }

        function sendMessage(session) {

            session.signal(
                {
                    data: $('.input-message').val(),
                },
                function (error) {
                    if (error) {
                        console.log("signal error ("
                            + error.name
                            + "): " + error.message);
                    } else {

                        console.log("signal sent. ");
                    }
                }
            );
        }

        function recievedMessages(session) {

            session.on("signal", function (event) {

                let _class = '';
                let html = '';
                if (event.from.id == session.connection.id) {
                  

                    if(event.data !=''){
                        html = `
                    <li>
                        <a href="#">
                            <img src="https://wastah.store/backend/public/images/users/164415763839385.jpg" alt="">
                            <div class="chat-desc">
                                <h5>${JSON.parse(event.from.data).name}</h5>
                                <p>${event.data}</p>
                            </div>
                        </a>
                    </li>
                    `;
                    }else{

                        $('.input-message').focus()
                    }
                   
             
                } else {

                    html = `
                    <li class="reverse-chat">
                        <a href="#">
                            <img src="https://wastah.store/backend/public/images/users/164415763839385.jpg" alt="">
                            <div class="chat-desc">
                                <h5>
                                ${JSON.parse(event.from.data).name}
                                </h5>
                                <p>${event.data}</p>
                            </div>
                        </a>
                    </li>
                    
                    `;
                  
                }


                $('.chat_box').append(html);
                $('.chat_box').scrollTop($('.chat_box')[0].scrollHeight);
                $('.input-message').val('');
            });

        }

        function disconnect(session) {


            toastr['success']('successsss', 'success')


            session.disconnect()

            window.location.href = '{!! url('/sessions') !!}'
        }

    </script>
@endpush